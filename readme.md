# How to configure
1. Build haproxy image: docker build -t <yourreponame>/haproxy .
2. Modify config file : haproxy.cfg - in this file youve got three apps: weba, webb, webc that are mounted to lb port 80, haproxy gets ports of those apps by container name, so when starting container please provide name that matches name in this config file.
3. Run your apps first - haproxy doesnt start if there is no containers running with this name, ex. docker run --name weba -P httpd
4. Run haproxy container with mouont volume where your haproxy.cfg file exists. - this should automatically start and serve requests to each of the apps based on roundrobin algorithm. ex: docker run -p 80:80 -p 70:70 -v ~/developement/awslatest/microblog/haproxy/:/usr/local/etc/haproxy/ tombart/haproxy 

 - /usr/local/etc/haproxy/ must be as above, haproxy server is looking for cofig in this directory
 - -p option exposes ports 80 and 70, 80 for apps and 70 for management

# Issues
- haproxy loads config only once on startup, that means if we restart one of the app container, it will never join load balancer unless we restart haproxy as well
